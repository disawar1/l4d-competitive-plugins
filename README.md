# Competitive L4D1 Plugins #

## Requirements ##
Some of these plugins may require include files, left4dhooks and r2compmod plugin installed on your server.

Links:
 
* [left4dhooks](https://forums.alliedmods.net/showthread.php?p=2684862)
* [l4d_lib](https://github.com/raziEiL/rotoblin2/tree/left4dhooks/left4dead/addons/sourcemod/scripting/include)
* [Rotoblin 2](https://github.com/raziEiL/rotoblin2/tree/left4dhooks)

*Port by raziEiL [disawar1]*