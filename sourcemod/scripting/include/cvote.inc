/*
 * ============================================================================
 *
 *  File:			cvote.inc
 *  Language:       SourcePawn
 *  Version:        1.1
 *
 *  Copyright (C) 2021 raziEiL [disawar1] <mr.raz4291@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#if defined _cvote_included
 #endinput
#endif
#define _cvote_included

forward void L4D_OnCustomVoteEnd(bool passed, const char[] issue);
native bool L4D_CustomVote(int client, const char[] issue);

public SharedPlugin __pl_cvote_lib =
{
	name = "cvote_lib",
	file = "cvote_lib.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_cvote_lib_SetNTVOptional()
{
	MarkNativeAsOptional("L4D_CustomVote");
}
#endif
