#include <sourcemod>
#include <sdktools>
#include <r2comp_api>
#undef REQUIRE_PLUGIN
#include <l4d_lib>
#include <cvote>

#define MATCHMODES_PATH         "configs/matchmodes.txt"
#define TAG "\x01[SM]"

public Plugin:myinfo =
{
	name = "[L4D] Vote match menu",
	author = "Scratchy [Laika], vintik, raziEiL [disawar1]",
	description = "Displays vote match menu and launches the match vote",
	version = "1.2",
	url = ""
};

new Handle:g_hModesKV = INVALID_HANDLE;
new String:g_sCfg[32];
new  Handle:resetLimits;

public OnPluginStart()
{
	decl String:sBuffer[128];
	g_hModesKV = CreateKeyValues("MatchModes");
	BuildPath(Path_SM, sBuffer, sizeof(sBuffer), MATCHMODES_PATH);
	if (!FileToKeyValues(g_hModesKV, sBuffer)) SetFailState("Couldn't load matchmodes.txt!");
	RegConsoleCmd("sm_match", MatchRequest);
	RegConsoleCmd("sm_load", MatchRequest);
	resetLimits = CreateConVar("sm_matchvote_playerlimits", "1", "Enable player limits for all configs", FCVAR_NOTIFY);
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	MarkCompNatives();
	RegPluginLibrary("l4d_matchvote");
	return APLRes_Success;
}

public L4D_OnCustomVoteEnd(bool passed, const char[] issue)
{
	if (passed && StrContains(issue, "Load '") != -1)
		ServerCommand("sm_forcematch %s", g_sCfg);
}

public Action:MatchRequest(client, args)
{
	if (!client) return Plugin_Handled;

	if (args > 0)
	{
		new String:sCfg[64], String:sName[64];
		GetCmdArg(1, sName, sizeof(sName));
		R2comp_GetMatchName(sCfg, sizeof(sCfg));
		if (!StrEqual(sCfg, sName, false))
		{
			if (IsMatchExists(sName))
			{
				if (StartMatchVote(client, sName))
				{
					strcopy(g_sCfg, sizeof(g_sCfg), sName);
				}
			}
			else ReplyToCommand(client, "%s This config name isn't exist", TAG);
			return Plugin_Handled;
		}
		else ReplyToCommand(client, "%s This config already loaded", TAG);
	}
	MatchModeMenu(client);
	return Plugin_Handled;
}

MatchModeMenu(client)
{
	new Handle:hMenu = CreateMenu(MatchModeMenuHandler);
	SetMenuTitle(hMenu, "Select match mode:");
	decl String:sBuffer[64], String:temp[12], flag;
	KvRewind(g_hModesKV);
	if (KvGotoFirstSubKey(g_hModesKV))
	{
		new bool:hidemode;
		do
		{
			hidemode = true;
			KvGetSectionName(g_hModesKV, sBuffer, sizeof(sBuffer));

			if (KvGotoFirstSubKey(g_hModesKV))
			{
				do
				{
					KvGetString(g_hModesKV, "gamemode", temp, 12, "");
					if (!IsGameModeMatch(temp)) continue;
					KvGetString(g_hModesKV, "flag", temp, 12, "");
					flag = ReadFlagString(temp);
					if (!(!strlen(temp) || flag && GetUserFlagBits(client) && CheckCommandAccess(client, "", flag))) continue;
					hidemode = false;
				}
				while (KvGotoNextKey(g_hModesKV));
				KvGoBack(g_hModesKV);
			}

			if (!hidemode)
				AddMenuItem(hMenu, sBuffer, sBuffer);
		}
		while (KvGotoNextKey(g_hModesKV));
	}
	DisplayMenu(hMenu, client, 20);
}

GetMinPlayers(const String:name[])
{
	KvRewind(g_hModesKV);
	if (KvGotoFirstSubKey(g_hModesKV))
	{
		do
		{
			if (KvJumpToKey(g_hModesKV, name))
			{
				return KvGetNum(g_hModesKV, "minplayers", 0);
			}
		}
		while (KvGotoNextKey(g_hModesKV, false));
	}
	return 0;
}

IsGameModeMatch(const String:gamemode[])
{
	static Handle:hGameMode;

	if (hGameMode == INVALID_HANDLE)
		hGameMode = FindConVar("mp_gamemode");

	decl String:sGameMode[12];
	GetConVarString(hGameMode, sGameMode, 12);
	return (!strlen(gamemode) || StrEqual(gamemode, sGameMode));
}

public ConfigsMenuHandler(Handle:menu, MenuAction:action, param1, param2)
{
	if (action == MenuAction_Select)
	{
		new String:sInfo[64], String:sBuffer[64];
		GetMenuItem(menu, param2, sInfo, sizeof(sInfo), _, sBuffer, sizeof(sBuffer));
		if (IsMatchExists(sInfo) && StartMatchVote(param1, sInfo, sBuffer))
		{
			strcopy(g_sCfg, sizeof(g_sCfg), sInfo);
		}
		else
		{
			MatchModeMenu(param1);
		}
	}
	if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
	if (action == MenuAction_Cancel)
	{
		MatchModeMenu(param1);
	}
}

public MatchModeMenuHandler(Handle:menu, MenuAction:action, param1, param2)
{
	if (action == MenuAction_Select)
	{
		new String:sInfo[64], String:sBuffer[64], String:sBuffer1[64];
		GetMenuItem(menu, param2, sInfo, sizeof(sInfo));
		KvRewind(g_hModesKV);
		if (KvJumpToKey(g_hModesKV, sInfo) && KvGotoFirstSubKey(g_hModesKV))
		{
			decl String:temp[12], flag;
			new Handle:hMenu = CreateMenu(ConfigsMenuHandler);
			FormatEx(sBuffer, sizeof(sBuffer), "Select %s config:", sInfo);
			R2comp_GetMatchName(sBuffer1, sizeof(sBuffer1));
			SetMenuTitle(hMenu, sBuffer);
			do
			{
				KvGetSectionName(g_hModesKV, sInfo, sizeof(sInfo));
				KvGetString(g_hModesKV, "name", sBuffer, sizeof(sBuffer));
				KvGetString(g_hModesKV, "gamemode", temp, 12, "");
				if (!IsGameModeMatch(temp)) continue;
				KvGetString(g_hModesKV, "flag", temp, 12, "");
				flag = ReadFlagString(temp);
				if (!(!strlen(temp) || flag && GetUserFlagBits(param1) && CheckCommandAccess(param1, "", flag))) continue;
				if (StrEqual(sInfo, sBuffer1)) AddMenuItem(hMenu, sInfo, sBuffer, ITEMDRAW_DISABLED);
				else AddMenuItem(hMenu, sInfo, sBuffer);
			}
			while (KvGotoNextKey(g_hModesKV));
			DisplayMenu(hMenu, param1, 20);
		}
		else
		{
			PrintToChat(param1, "%s No configs with such mode were found.", TAG);
			MatchModeMenu(param1);
		}
	}
	if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

bool:StartMatchVote(client, const String:cfgname[], const String:matchname[] = "")
{
	if (GetConVarBool(resetLimits))
	{
		new minplayers = GetMinPlayers(cfgname);

		if (minplayers > GetPlayerCount(false))
		{
			PrintToChat(client, "%s Not enough players. The \x04%s\x01 config requires \x04%i\x01 players!", TAG, strlen(matchname) ? matchname : cfgname, minplayers);
			return false;
		}
	}

	decl String:text[96];
	FormatEx(text, sizeof(text), "Load '%s' config?", strlen(matchname) ? matchname : cfgname);
	return L4D_CustomVote(client, text);
}

// IsSpectators не считать спектров?
GetPlayerCount(bool:IsSpectators = true)
{
	new j=0;
	for (new i = 1; i<=MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i) && (IsSpectators ? GetClientTeam(i) != 1 : true))
		{
			j++
		}
	}
//	PrintToChatAll("Account of players is %i", j);
	return j;
}

bool:IsMatchExists(const String:sName[])
{
	if (!strlen(sName) || StrContains(sName, ".") != -1 || StrContains(sName, "\"") != -1)
		return false;

	decl String:sBuildPatch[96];
	FormatEx(sBuildPatch, sizeof(sBuildPatch), "cfg/cfgroto/%s", sName);

	return DirExists(sBuildPatch);
}