#pragma semicolon 1
// l4d2 Bill 0, Zoey 1, Louis 2, Francis 3, Nick 0, Ro 1, Coach 2, Ellis 3
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

#define DEBUG_MODE              0

#define TEAM_SPECTATOR          1
#define TEAM_SURVIVOR           2
#define TEAM_INFECTED           3

#define SEQ_FLIGHT_LOUIS     544
#define SEQ_FLIGHT_BILL     493 //544
#define SEQ_FLIGHT_FRANCIS  545
#define SEQ_FLIGHT_ZOEY      526

#define TIMER_MAXCHECKTIME        5.0
#define TIMER_CHECKPUNCH        0.025   // interval for checking 'flight' of punched survivors
#define TELEFIX_DOWN_DISTANCE   20.0    // how far to teleport player down to get them out of ceiling
#define ROOF_DISTANCE   65.0                // dist between client and roof

#define PUNCH_WAIT  0
#define PUNCH_CHECK 1
#define PUNCH_STOP  2

new     bool:       g_bLateLoad                                 = false;
new     bool:       g_bPlayerFlight         [MAXPLAYERS + 1];                           // is a player in (potentially stuckable) punched flight?
new     Float:      g_fPlayerStuck          [MAXPLAYERS + 1];                           // when did the (potential) 'stuckness' occur?
new     Float:      g_fPlayerLocation       [MAXPLAYERS + 1][3];                        // where was the survivor last during the flight?
new     bool:       g_bEndCheck               [MAXPLAYERS+1];
new     Handle:     g_hCvarPluginEnabled                        = INVALID_HANDLE;       // convar: enable fix
new     Handle:     g_hCvarDeStuckTime                          = INVALID_HANDLE;       // convar: how long to wait and de-stuckify a punched player
new     Float:      g_fCvarDeStuckTime;
new     bool:       g_bCvarPluginEnable;


/*
    -----------------------------------------------------------------------------------------------------------------------------------------------------


    Here's the idea:
        - When a tank punches a survivors stuck in a ceiling, the survivor also gets
            stuck in a 'flying' state, lets in the air and all.
        - After a tank punch that lands on a survivor (ie. does damage?),
            do a check on the m_nSequence of the survivor
            - if it does not reach 634 within [0.5 or so] seconds, it wasn't a problematic punch
            - if it does, keep checking it
                if it doesn't change for [2 or so] seconds AND the survivor's location doesn't change,
                it's a stuckpunch - teleport survivor a bit lower and see what happens...


    Changelog
    ---------
        0.3
            - L4D port (Thanks to inok_nq for helping testing)

        0.2.1
            -   Working fix (as tested by epilimic). Cleaned up code.

        0.1.1
            -   First version that attempts a detect + fix combo.
                Does a sequence, time and location-based stuckness detection and teleports player downwards
                for an attempt at a fix.


    -----------------------------------------------------------------------------------------------------------------------------------------------------
 */


public Plugin:myinfo =
{
    name =          "[L4D] Tank Punch Ceiling Stuck Fix",
    author =        "Tabun, raziEiL [disawar1]",
    description =   "Fixes the problem where tank-punches get a survivor stuck in the roof.",
    version =       "0.3",
    url =           "nope"
}

/* -------------------------------
 *      Init
 * ------------------------------- */

public OnPluginStart()
{
    // cvars
    g_hCvarPluginEnabled = CreateConVar(    "sm_punchstuckfix_enabled",         "1",        "Enable the fix.", FCVAR_NOTIFY, true, 0.0, true, 1.0);
    g_hCvarDeStuckTime = CreateConVar(      "sm_punchstuckfix_unstucktime",     "0.5",      "How many seconds to wait before detecting and unstucking a punched motionless player.", FCVAR_NOTIFY, true, 0.05, false);

    HookConVarChange(g_hCvarPluginEnabled, OnCvarChange_PluginEnablede);
    HookConVarChange(g_hCvarDeStuckTime, OnCvarChange_DeStuckTime);
    g_bCvarPluginEnable = GetConVarBool(g_hCvarPluginEnabled);
    g_fCvarDeStuckTime = GetConVarFloat(g_hCvarDeStuckTime);

        // hook already existing clients if loading late
    if (g_bCvarPluginEnable && g_bLateLoad) {
        for (new i = 1; i < MaxClients+1; i++) {
            if (IsClientInGame(i)) {
                SDKHook(i, SDKHook_OnTakeDamage, OnTakeDamage);
            }
        }
    }
}

/* --------------------------------------
 *      General hooks / events
 * -------------------------------------- */
static g_bTempBlock[MAXPLAYERS+1];

public OnClientPutInServer(client)
{
    if (g_bCvarPluginEnable && client){

        g_bTempBlock[client] = false;
        SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
    }
}

/* --------------------------------------
 *     GOT MY EYES ON YOU, PUNCH
 * -------------------------------------- */

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damageType, &weapon, Float:damageForce[3], Float:damagePosition[3])
{
    // only check player-to-player damage
    if (!inflictor || !IsValidEntity(inflictor) || !IsClient(victim) || g_bTempBlock[victim] ||
        !IsSurvivor(victim) || !IsClientAndInGame(attacker))  { return Plugin_Continue; }

    decl String:classname[64];
    if (attacker == inflictor)                                              // for claws
    {
        GetClientWeapon(inflictor, classname, sizeof(classname));
    }
    else
    {
        GetEntityClassname(inflictor, classname, sizeof(classname));         // for tank punch/rock
    }
    PrintDebug("[test] %s", classname);
    // only check tank punch (also rules out anything but infected-to-survivor damage)
    if (!StrEqual("weapon_tank_claw", classname)) { return Plugin_Continue; }

    // tank punched survivor, check the result

    PF_ResetVars(victim);

    g_bTempBlock[victim] = true;
    CreateTimer(TIMER_CHECKPUNCH, Timer_CheckPunch, victim, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
    CreateTimer(TIMER_MAXCHECKTIME, Timer_EndCheckPunch, victim, TIMER_FLAG_NO_MAPCHANGE);

    return Plugin_Continue;
}

public Action:Timer_EndCheckPunch(Handle:hTimer, any:client)
{
    g_bEndCheck[client] = true;
}

public Action: Timer_CheckPunch(Handle:hTimer, any:client)
{
    // stop the timer when we no longer have a proper client
    if (!IsSurvivor(client) || g_bEndCheck[client]){

        PrintDebug("[test] %d - flight end (natural)", client);
        g_bTempBlock[client] = false;
        return Plugin_Stop;
    }

    // get current animation frame and location of survivor

    decl iSeqFlight;
    switch (GetEntProp(client, Prop_Send, "m_survivorCharacter"))
    {
        case 0:
            iSeqFlight = SEQ_FLIGHT_BILL;
        case 1:
            iSeqFlight = SEQ_FLIGHT_ZOEY;
        case 2:
            iSeqFlight = SEQ_FLIGHT_FRANCIS;
        case 3:
            iSeqFlight = SEQ_FLIGHT_LOUIS;
        default:
            return Plugin_Stop;
    }

    new iSeq = GetEntProp(client, Prop_Send, "m_nSequence");

    // if the player is not in flight, check if they are
    if (iSeq == iSeqFlight)
    {
        PrintDebug("[test] PUNCH_CHECK");
        decl Float: vOrigin[3];
        GetClientAbsOrigin(client, vOrigin);

        if (!g_bPlayerFlight[client])
        {
            // if the player is not detected as in punch-flight, they are now
            g_bPlayerFlight[client] = true;
            g_fPlayerLocation[client] = vOrigin;

            PrintDebug("[test] %N - flight start [seq:%4i][loc:%.f %.f %.f]", client, iSeq, vOrigin[0], vOrigin[1], vOrigin[2]);
        }
        else
        {
            // if the player is in punch-flight, check location / difference to detect stuckness
            if (!GetVectorDistance(g_fPlayerLocation[client], vOrigin)) {

                // are we /still/ in the same position? (ie. if stucktime is recorded)
                if (!g_fPlayerStuck[client])
                {
                    g_fPlayerStuck[client] = GetTickedTime();

                    PrintDebug("[test] %N - stuck start [loc:%.f %.f %.f]", client, vOrigin[0], vOrigin[1], vOrigin[2]);
                }
                else
                {
                    PrintDebug("[test] tickettime %.2f, stucktime %.2f, maxstucktime %.2f", GetTickedTime(), GetTickedTime() - g_fPlayerStuck[client], g_fCvarDeStuckTime);
                    if (GetTickedTime() - g_fPlayerStuck[client] >= g_fCvarDeStuckTime)
                    {
                        // time passed, player is stuck! fix.

                        if (IsStuckInRoof(vOrigin)){ // ���� ��������� ����� ������� � �������� ������ 75

                            new Float:fFloor = GetFloorDist(vOrigin); // ���� ��������� ����� ������� � ����� ������ ��� ���������� �� ������� ������ ���������� ����
                            new Float:fTeleDownDist = (TELEFIX_DOWN_DISTANCE > fFloor ? TELEFIX_DOWN_DISTANCE - fFloor : TELEFIX_DOWN_DISTANCE);
                            vOrigin[2] -= fTeleDownDist;
                            TeleportEntity(client, vOrigin, NULL_VECTOR, NULL_VECTOR);
                            PrintDebug("[test] %N - stuckness FIX triggered! Tel down %.1f", client, fTeleDownDist);
                            PrintToChatAll("[TankPunchFix] %N - stuckness FIX triggered!", client);
                        }
                        else
                            PrintDebug("[test] %N - stuckness triggered but false detected!", client);

                        PF_ResetVars(client);
                        g_bTempBlock[client] = false;

                        return Plugin_Stop;
                    }
                }
            }
            else
            {
                // if we were detected as stuck, undetect
                if (g_fPlayerStuck[client])
                {
                    g_fPlayerStuck[client] = 0.0;

                    PrintDebug("[test] %N - stuck end (previously detected, now gone) [loc:%.f %.f %.f]", client, vOrigin[0], vOrigin[1], vOrigin[2]);
                }
            }
        }
    }

    return Plugin_Continue;
}

bool:IsStuckInRoof(const Float:pos[3])
{
    new Handle:trace = TR_TraceRayFilterEx(pos, Float:{ 270.0, 0.0, 0.0 }, CONTENTS_SOLID, RayType_Infinite, TraceEntityFilter);

    if (TR_DidHit(trace)){

        if (TR_GetEntityIndex(trace) > 0){

            CloseHandle(trace);
            return false;
        }

        decl Float:posEnd[3];
        TR_GetEndPosition(posEnd, trace);

        new Float:fDist = GetVectorDistance(pos, posEnd);

        PrintDebug("Roof dist %.1f, endpos %.1f %.1f %.1f", fDist, posEnd[0], posEnd[1], posEnd[2]);

        return fDist < ROOF_DISTANCE;
    }

    CloseHandle(trace);
    return false;
}

Float:GetFloorDist(const Float:pos[3])
{
    new Handle:trace = TR_TraceRayFilterEx(pos, Float:{ 90.0, 0.0, 0.0 }, CONTENTS_SOLID, RayType_Infinite, TraceEntityFilter);

    if (TR_DidHit(trace)){

        if (TR_GetEntityIndex(trace) > 0){

            CloseHandle(trace);
            return 0.0;
        }

        decl Float:posEnd[3];
        TR_GetEndPosition(posEnd, trace);

        return GetVectorDistance(pos, posEnd);
    }

    CloseHandle(trace);
    return 0.0;
}

public bool:TraceEntityFilter(entity, contentsMask)
{
    return entity == 0;
}

PF_ResetVars(i)
{
    g_bPlayerFlight[i] = false;
    g_fPlayerStuck[i] = 0.0;
    g_fPlayerLocation[i][0] = 0.0;
    g_fPlayerLocation[i][1] = 0.0;
    g_fPlayerLocation[i][2] = 0.0;
    g_bEndCheck[i] = false;
}

public PrintDebug(const String:Message[], any:...)
{
    #if DEBUG_MODE
        decl String:DebugBuff[256];
        VFormat(DebugBuff, sizeof(DebugBuff), Message, 2);
        //LogMessage(DebugBuff);
        //PrintToServer(DebugBuff);
        PrintToChatAll(DebugBuff);
    #endif
}

TP_ToogleHook(bool:bHook)
{
    for (new i = 1; i <= MaxClients; i++){

        if (!IsClientInGame(i)) continue;

        if (bHook)
            SDKHook(i, SDKHook_OnTakeDamage, OnTakeDamage);
        else
            SDKUnhook(i, SDKHook_OnTakeDamage, OnTakeDamage);
    }
}

public OnCvarChange_PluginEnablede(Handle:hndl, const String:oldValue[], const String:newValue[])
{
    if (StrEqual(oldValue, newValue)) return;

    g_bCvarPluginEnable = GetConVarBool(hndl);

    if (!StringToInt(oldValue))
        TP_ToogleHook(true);
    else if (!g_bCvarPluginEnable)
        TP_ToogleHook(false);
}

public OnCvarChange_DeStuckTime(Handle:hndl, const String:oldValue[], const String:newValue[])
{
    if (!StrEqual(oldValue, newValue))
        g_fCvarDeStuckTime = GetConVarFloat(hndl);
}
