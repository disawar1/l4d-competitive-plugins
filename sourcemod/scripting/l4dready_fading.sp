#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <l4d_lib>
#define COLOR_CORRECTION "materials/correction/thirdstrike.raw" // cc_blackwhiteblue
//#define ColorCorrection_Pwl "materials/correction/cc_blackwhiteblue.pwl.raw"

new Ent = INVALID_ENT_REFERENCE, bool:g_bIsEnabled;

public Plugin:myinfo =
{
	name = "[L4D] Ready Up Fading",
	author = "Scratchy [Laika] & raziEiL [disawar1]",
	description = "Add color correction effect until round started",
	version = "1.0",
	url = "http://steamcommunity.com/groups/Allianc-e"
}

public OnPluginStart()
{
	HookEvent("round_start", RoundStart, EventHookMode_PostNoCopy);
	RegAdminCmd("sm_togglecc", Command_ToggleCC, ADMFLAG_ROOT);
}

public Action:Command_ToggleCC(client, args)
{
	new entity = EntRefToEntIndex(Ent);
	if (entity != INVALID_ENT_REFERENCE){

		AcceptEntityInput(entity, g_bIsEnabled ? "Disable" : "Enable");
		g_bIsEnabled = !g_bIsEnabled;
	}
	return Plugin_Handled;
}

public OnMapStart()
{
	PrecacheModel(COLOR_CORRECTION, true);
}

public RoundStart(Handle:event, const String:sName[], bool:DontBroadCast)
{
	CreateTimer(0.6, Timer_Delay, TIMER_FLAG_NO_MAPCHANGE);
}

public Action:Timer_Delay(Handle:timer)
{
	new entity = CreateColorCorrectionEntity();

	if (L4DReady_IsReadyMode() && entity != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(entity, "Enable");
	}
	g_bIsEnabled = true;
}
/*
public OnMapStart()
{
	AddFileToDownloadsTable(COLOR_CORRECTION);
	AddFileToDownloadsTable(ColorCorrection_Pwl);
}
*/
public L4DReady_OnRoundIsLive()
{
	new entity = EntRefToEntIndex(Ent);
	if (entity != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(entity, "Disable");
	}
	g_bIsEnabled = false;
}

CreateColorCorrectionEntity()
{
	Ent = CreateEntityByName("color_correction");
	if (Ent != -1)
	{
		DispatchKeyValue(Ent, "filename", COLOR_CORRECTION);
		DispatchKeyValue(Ent, "StartDisabled", "1");
		DispatchKeyValue(Ent, "minfalloff", "-1");
		DispatchKeyValue(Ent, "maxfalloff", "-1");
		DispatchKeyValue(Ent, "exclusive", "0");
		DispatchKeyValue(Ent, "spawnflags", "0");
		DispatchKeyValue(Ent, "fadeInDuration", "2");
		DispatchKeyValue(Ent, "fadeOutDuration", "2");
		DispatchSpawn(Ent);
		ActivateEntity(Ent);
		Ent = EntIndexToEntRef(Ent);
	}
	return Ent;
}
