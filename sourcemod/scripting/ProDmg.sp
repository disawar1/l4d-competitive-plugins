#define PLUGIN_VERSION	"1.5"

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

#define debug		0

#define TAG				"[ProDMG]"

#define NULL					-1
#define BOSSES				2
#define TANK_PASS_TIME		(g_fCvarTankSelectTime + 1.0)

#define CVAR_FLAGS			FCVAR_NOTIFY

enum DATA
{
	INDEX,
	DMG,
	WITCH = 0,
	TANK
}

public Plugin:myinfo =
{
	name = "ProDMG",
	author = "raziEiL [disawar1]",
	description = "Bosses dealt damage announcer.",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/raziEiL"
}

forward TP_OnTankPass(old_tank, new_tank);

static		Handle:g_hTankHealth, Handle:g_hVsBonusHealth, Handle:g_hDifficulty, Handle:g_hGameMode, bool:g_bCvarSkipBots, bool:g_bCvar1v1Mode, g_iCvarHealth[BOSSES],
			g_iDamage[MPS][MPS][BOSSES], g_iWitchIndex[MPS], g_iTotalDamage[MPS][BOSSES],
			bool:bTempBlock, g_iLastKnownTank, bool:g_bTankInGame, Handle:g_hTrine, g_iCvarFlags, g_iCvarPrivateFlags,
			bool:g_bNoHrCrown[MPS], g_iWitchCount, bool:g_bCvarRunAway, g_iWitchRef[MPS];

public OnPluginStart()
{
	LoadTranslations("l4d_competitive.phrases");

	g_hTankHealth		= FindConVar("z_tank_health");
	g_hVsBonusHealth	= FindConVar("versus_tank_bonus_health");
	g_hDifficulty		= FindConVar("z_difficulty");
	g_hGameMode			= FindConVar("mp_gamemode");

	new Handle:hCvarWitchHealth			= FindConVar("z_witch_health");
	new Handle:hCvarSurvLimit			= FindConVar("survivor_limit");

	new Handle:hCvarFlags		= CreateConVar("prodmg_announce_flags",		"0", "What stats get printed to chat. Flags: 0=disabled, 1=witch, 2=tank, 3=all", CVAR_FLAGS, true, 0.0, true, 3.0);
	new Handle:hCvarSkipBots	= CreateConVar("prodmg_ignore_bots",			"0", "If set, bots stats won't get printed to chat", CVAR_FLAGS, true, 0.0, true, 1.0);
	new Handle:hCvarPrivate		= CreateConVar("prodmg_announce_private",	"0", "If set, stats wont print to public chat. Flags (add together): 0=disabled, 1=witch, 2=tank, 3=all", CVAR_FLAGS, true, 0.0, true, 3.0);
	new Handle:hCvarRunAway		= CreateConVar("prodmg_failed_crown",		"1", "If set, witch stats at round end won't print if she isn't killed", CVAR_FLAGS, true, 0.0, true, 1.0);

	g_iCvarHealth[TANK]	= RoundFloat(GetConVarFloat(g_hTankHealth) * (IsVersusGameMode() ? GetConVarFloat(g_hVsBonusHealth) : GetCoopMultiplie()));
	g_iCvarHealth[WITCH]	= GetConVarInt(hCvarWitchHealth);
	g_bCvar1v1Mode			= GetConVarInt(hCvarSurvLimit) == 1 ? true : false;
	g_iCvarFlags				= GetConVarInt(hCvarFlags);
	g_bCvarSkipBots			= GetConVarBool(hCvarSkipBots);
	g_bCvarRunAway			= GetConVarBool(hCvarRunAway);

	HookConVarChange(g_hDifficulty,			OnConvarChange_TankHealth);
	HookConVarChange(g_hTankHealth,			OnConvarChange_TankHealth);
	HookConVarChange(g_hGameMode,			OnConvarChange_TankHealth);
	HookConVarChange(g_hVsBonusHealth,		OnConvarChange_TankHealth);
	HookConVarChange(hCvarWitchHealth,		OnConvarChange_WitchHealth);
	HookConVarChange(hCvarSurvLimit,		OnConvarChange_SurvLimit);
	HookConVarChange(hCvarFlags,				OnConvarChange_Flags);
	HookConVarChange(hCvarSkipBots,			OnConvarChange_SkipBots);
	HookConVarChange(hCvarPrivate,			OnConvarChange_Private);
	HookConVarChange(hCvarRunAway,			OnConvarChange_RunAway);

	HookEvent("round_start",			PD_ev_RoundStart,		EventHookMode_PostNoCopy);
	HookEvent("round_end",			PD_ev_RoundEnd,			EventHookMode_PostNoCopy);
	HookEvent("tank_spawn",			PD_ev_TankSpawn,		EventHookMode_PostNoCopy);
	HookEvent("witch_spawn",			PD_ev_WitchSpawn);
	HookEvent("tank_frustrated",		PD_ev_TankFrustrated);
	HookEvent("witch_killed",			PD_ev_WitchKilled);
	HookEvent("entity_killed",		PD_ev_EntityKilled);
	HookEvent("player_hurt",			PD_ev_PlayerHurt);
	HookEvent("infected_hurt",		PD_ev_InfectedHurt);
	HookEvent("player_bot_replace",	PD_ev_PlayerBotReplace);

	g_hTrine = CreateTrie();
}

public Action:PD_ev_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	bTempBlock = false;
	g_bTankInGame = false;
	g_iLastKnownTank = 0;
	g_iWitchCount = 0;
	ClearTrie(g_hTrine);

	for (new i; i < MPS; i++){

		for (new elem; elem < MPS; elem++){

			g_iDamage[i][elem][TANK] = 0;
			g_iDamage[i][elem][WITCH] = 0;
		}

		g_iTotalDamage[i][TANK] = 0;
		g_iTotalDamage[i][WITCH] = 0;
		g_iWitchRef[i] = INVALID_ENT_REFERENCE;
		g_iWitchIndex[i] = 0;
		g_bNoHrCrown[i] = false;
	}
}

public Action:PD_ev_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (bTempBlock || !g_iCvarFlags) return;

	bTempBlock = true;
	g_bTankInGame = false;

	decl String:sName[32];

	if (g_iCvarFlags & (1 << _:TANK)){

		new iTank = IsTankInGame();
		if (iTank && !g_iTotalDamage[iTank][TANK]){

			GetClientName(iTank, sName, 32);
			PrintToChatAll("%t", "Prodmg #1", IsFakeClient(iTank) ? "AI" : sName, g_iCvarHealth[TANK] - (g_iCvarHealth[TANK] - GetClientHealth(iTank)));
		}
	}

	for (new i; i <= MaxClients; i++){

		if (g_iTotalDamage[i][TANK]){

			if (!IsClientInGame(i))
				PrintToChatAll("%t", "Prodmg #2", g_iCvarHealth[TANK] - g_iTotalDamage[i][TANK]);

			else{

				GetClientName(i, sName, 32);
				PrintToChatAll("%t", "Prodmg #1", IsFakeClient(i) ? "AI" : sName, g_iCvarHealth[TANK] - g_iTotalDamage[i][TANK]);
			}

			PrintDamage(i, true, true);
		}
		if (g_iTotalDamage[i][WITCH]){

			if (g_bCvarRunAway && g_iWitchRef[i] != INVALID_ENT_REFERENCE && EntRefToEntIndex(g_iWitchRef[i]) == INVALID_ENT_REFERENCE) continue;

			PrintToChatAll("%t", "Prodmg #3", g_iCvarHealth[WITCH] - g_iTotalDamage[i][WITCH]);
			PrintDamage(i, false, true);
		}
	}
}

// Tank
public OnClientPutInServer(client)
{
	if (g_bTankInGame && g_iCvarFlags & (1 << _:TANK) && client){

		if (!IsFakeClient(client)){

			decl String:sName[32], String:sIndex[16];
			GetClientName(client, sName, 32);
			IntToString(client, sIndex, 16);
			SetTrieString(g_hTrine, sIndex, sName);
		}
		else
			CreateTimer(0.0, PD_t_CheckIsInf, client);
	}
}

public Action:PD_t_CheckIsInf(Handle:timer, any:client)
{
	if (IsClientInGame(client) && IsFakeClient(client)){

		decl String:sName[32];
		GetClientName(client, sName, 32);

		if (StrContains(sName, "Smoker") != -1 || StrContains(sName, "Boomer") != -1 || StrContains(sName, "Hunter") != -1) return;

		decl String:sIndex[16];
		IntToString(client, sIndex, 16);
		SetTrieString(g_hTrine, sIndex, sName);
	}
}

public Action:PD_ev_TankSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_bTankInGame && g_iCvarFlags & (1 << _:TANK)){

		decl String:sName[32], String:sIndex[16];

		for (new i = 1; i <= MaxClients; i++){

			if (!IsClientInGame(i) || (IsFakeClient(i) && GetClientTeam(i) == 3)) continue;

			IntToString(i, sIndex, 16);
			GetClientName(i, sName, 32);
			SetTrieString(g_hTrine, sIndex, sName);

			#if debug
				LogMessage("push to trine. %s (%s)", sIndex, sName);
			#endif
		}
	}

	g_bTankInGame = true;
}

public Action:PD_ev_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (bTempBlock || !(g_iCvarFlags & (1 << _:TANK))) return;

	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

	if (IsClientAndInGame(victim) && IsClientAndInGame(attacker) && GetClientTeam(attacker) == 2  && GetClientTeam(victim) == 3){

		if (!IsPlayerTank(victim) || g_iTotalDamage[victim][TANK] == g_iCvarHealth[TANK]) return;

		if (g_iLastKnownTank)
			CloneStats(victim);

		new iDamage = GetEventInt(event, "dmg_health");

		g_iDamage[attacker][victim][TANK] += iDamage;
		g_iTotalDamage[victim][TANK] += iDamage;

		#if debug
			LogMessage("#1. total %d dmg %d (%N, health %d)", g_iTotalDamage[victim][TANK], iDamage, victim, GetEventInt(event, "health"));
		#endif

		CorrectDmg(attacker, victim, true);
	}
}

CloneStats(client)
{
	if (client && client != g_iLastKnownTank){

		#if debug
			LogMessage("clone tank stats %N -> %N", g_iLastKnownTank, client);
		#endif

		for (new i; i <= MaxClients; i++){

			if (g_iDamage[i][g_iLastKnownTank][TANK]){

				g_iDamage[i][client][TANK] = g_iDamage[i][g_iLastKnownTank][TANK];
				g_iDamage[i][g_iLastKnownTank][TANK] = 0;
			}
		}

		g_iTotalDamage[client][TANK] = g_iTotalDamage[g_iLastKnownTank][TANK];
		g_iTotalDamage[g_iLastKnownTank][TANK] = 0;
	}
	#if debug
	else
		LogMessage("don't clone tank stats %N -> %N", g_iLastKnownTank, client);
	#endif

	g_iLastKnownTank = 0;
}

public Action:PD_ev_EntityKilled(Handle:event, const String:name[], bool:dontBroadcast)
{
	decl client;
	if (!bTempBlock && g_bTankInGame && g_iCvarFlags & (1 << _:TANK) && IsPlayerTank((client = GetEventInt(event, "entindex_killed"))))
		CreateTimer(1.5, PD_t_FindAnyTank, client, TIMER_FLAG_NO_MAPCHANGE);
}

public Action:PD_t_FindAnyTank(Handle:timer, any:client)
{
	#if debug
		LogMessage("entity killed %d fired!", client);
	#endif

	if (!IsTankInGame()){

		g_bTankInGame = false;

		if (g_iTotalDamage[client][TANK])
			PrintDamage(client, true);
		else if (!g_bCvar1v1Mode)// wtf?
			PrintToChatAll("%t", "Prodmg #4");
	}
	#if debug
	else
		LogMessage("tank in game");
	#endif
}

IsTankInGame(exclude = 0)
{
	for (new i = 1; i <= MaxClients; i++)
		if (exclude != i && IsClientInGame(i) && GetClientTeam(i) == 3 && IsPlayerTank(i) && IsPlayerAlive(i) && !IsIncapacitated(i))
			return i;

	return 0;
}

public Action:PD_ev_PlayerBotReplace(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (bTempBlock || g_bCvar1v1Mode || !(g_iCvarFlags & (1 << _:TANK))) return;

	// tank leave?
	new client = GetClientOfUserId(GetEventInt(event, "player"));

	if (!g_iLastKnownTank && g_iTotalDamage[client][TANK]){

		#if debug
			LogMessage("tank %N leave inf team!", client);
		#endif

		g_iLastKnownTank = client;
		CloneStats(GetClientOfUserId(GetEventInt(event, "bot")));
	}
}

public Action:PD_ev_TankFrustrated(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (bTempBlock || !(g_iCvarFlags & (1 << _:TANK))) return;

	#if debug
		LogMessage("TankFrustrated fired (pass time %f sec)", TANK_PASS_TIME);
	#endif

	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!IsClientInGame(client)) return;

	if (!g_bCvar1v1Mode){

		g_iLastKnownTank = client;
		return;
	}

	// 1v1
	PrintToChatAll("%t", "Prodmg #5", client);
	PrintToChatAll("%t", "Prodmg #6", g_iCvarHealth[TANK] - g_iTotalDamage[client][TANK]);

	if (g_iTotalDamage[client][TANK])
		PrintDamage(client, true, true);
}

public TP_OnTankPass(old_tank, new_tank)
{
	if (bTempBlock || !(g_iCvarFlags & (1 << _:TANK)) || g_bCvar1v1Mode) return;

	g_iLastKnownTank = old_tank;
}

// Witch
public Action:PD_ev_WitchSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	CreateTimer(0.5, PD_t_EnumThisWitch, EntIndexToEntRef(GetEventInt(event, "witchid")), TIMER_FLAG_NO_MAPCHANGE);
}

public Action:PD_t_EnumThisWitch(Handle:timer, any:entity)
{
	new ref = entity;
	if ((entity = EntRefToEntIndex(entity)) != INVALID_ENT_REFERENCE && g_iWitchCount < MPS){

		g_iWitchRef[g_iWitchCount] = ref;

		decl String:sWitchName[8];
		FormatEx(sWitchName, 8, "%d", g_iWitchCount++);
		DispatchKeyValue(entity, "targetname", sWitchName);
	}
}

public Action:PD_ev_InfectedHurt(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (bTempBlock || !(g_iCvarFlags & (1 << _:WITCH))) return;

	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

	decl iWitchEnt;
	if (IsWitch((iWitchEnt = GetEventInt(event, "entityid"))) && IsClientAndInGame(attacker) && GetClientTeam(attacker) == 2){

		new iIndex = GetWitchIndex(iWitchEnt);
		if (iIndex == NULL) return;

		if (!g_bNoHrCrown[iIndex] && GetEventInt(event, "amount") != 90)
			g_bNoHrCrown[iIndex] = true;

		if (g_iTotalDamage[iIndex][WITCH] == g_iCvarHealth[WITCH]) return;

		new iDamage = GetEventInt(event, "amount");

		g_iDamage[attacker][iIndex][WITCH] += iDamage;
		g_iTotalDamage[iIndex][WITCH] += iDamage;

		#if debug
			LogMessage("%d (Witch: indx %d, elem %d)", g_iTotalDamage[iIndex][WITCH], iWitchEnt, iIndex);
		#endif

		CorrectDmg(attacker, iIndex, false);
	}
}

GetWitchIndex(entity)
{
	decl String:sWitchName[8];
	GetEntPropString(entity, Prop_Data, "m_iName", sWitchName, 8);
	if (strlen(sWitchName) != 1) return -1;

	return StringToInt(sWitchName);
}
// ---

CorrectDmg(attacker, iIndex, bool:bTankBoss)
{
	if (g_iTotalDamage[iIndex][bTankBoss] > g_iCvarHealth[bTankBoss]){

		new iDiff = g_iTotalDamage[iIndex][bTankBoss] - g_iCvarHealth[bTankBoss];

		#if debug
			LogMessage("dmg corrected %d. total dmg %d", iDiff, g_iTotalDamage[iIndex][bTankBoss]);
		#endif

		g_iDamage[attacker][iIndex][bTankBoss] -= iDiff;
		g_iTotalDamage[iIndex][bTankBoss] -= iDiff;
	}
}

public Action:PD_ev_WitchKilled(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!(g_iCvarFlags & (1 << _:WITCH))) return;

	new iIndex = GetWitchIndex(GetEventInt(event, "witchid"));
	if (iIndex == NULL || !g_iTotalDamage[iIndex][WITCH]) return;

	PrintDamage(iIndex, false, _, !g_bNoHrCrown[iIndex] ? 2 : GetEventInt(event, "oneshot"));
	g_bNoHrCrown[iIndex] = false;
}

PrintDamage(iIndex, bool:bTankBoss, bool:bLoose = false, iCrownTech = 0)
{
	decl iClient[MPS][BOSSES];
	new iSurvivors;

	for (new i = 1; i <= MaxClients; i++){

		if (!g_iDamage[i][iIndex][bTankBoss]) continue;

		if (!bTankBoss && IsClientInGame(i) || bTankBoss){

			if ((g_bCvarSkipBots && IsClientInGame(i) && !IsFakeClient(i)) || !g_bCvarSkipBots){

				iClient[iSurvivors][INDEX] = i;
				iClient[iSurvivors][DMG] = g_iDamage[i][iIndex][bTankBoss];
				iSurvivors++;
			}
		}
		// reset var
		g_iDamage[i][iIndex][bTankBoss] = 0;
	}
	if (!iSurvivors) return;

	if (iSurvivors == 1 && !bLoose){

		if (bTankBoss)
			PrintToChatAll("%t", "Prodmg #7", iClient[0][INDEX], iClient[0][DMG]);
		else{

			if (IsIncapacitated(iClient[0][INDEX]))
				PrintToChatAll("\x01%N '\x03Jerkstored\x01' a cr0wn (failed)", iClient[0][INDEX]);
			else
				PrintToChatAll("%t", "Prodmg #15", iClient[0][INDEX], !iCrownTech ? "Prodmg #16" : iCrownTech == 1 ? "Prodmg #18" : "Prodmg #17");
		}
	}
	else {

		new Float:fTotalDamage = float(g_iCvarHealth[bTankBoss]);

		SortCustom2D(iClient, iSurvivors, SortFuncByDamageDesc);

		if (!bLoose && !(g_iCvarPrivateFlags & (1 << (bTankBoss ? 1 : 0))))
			PrintToChatAll("%t", "Prodmg #8", bTankBoss ? "Prodmg #9" : "Prodmg #10", g_iTotalDamage[iIndex][bTankBoss]);

		new iTranslateReq;

		if (bTankBoss){

			decl String:sName[48], client, bool:bInGame;

			for (new i; i < iSurvivors; i++){

				iTranslateReq = 0;
				client = iClient[i][INDEX];

				if ((bInGame = IsSurvivor(client)))
					GetClientName(client, sName, 48);
				else {

					IntToString(client, sName, 48);

					if (GetTrieString(g_hTrine, sName, sName, 48)){

						Format(sName, 48, "%s", sName);
						iTranslateReq = 1;

					}
					else {

						sName = "";
						iTranslateReq = 2;
					}
				}
					// private
				if (g_iCvarPrivateFlags & (1 << _:TANK)){

					if (bInGame)
						PrintToChat(client, "%t", "Prodmg #11", g_iTotalDamage[iIndex][bTankBoss], i + 1, iClient[i][DMG], (float(iClient[i][DMG]) / fTotalDamage) * 100.0);
				}
				else  {// public

					if (iTranslateReq)
						PrintToChatAll("%d (%.0f%%): %s %t", iClient[i][DMG], (float(iClient[i][DMG]) / fTotalDamage) * 100.0, sName, iTranslateReq == 1 ? "Prodmg #12" : "Prodmg #13");
					else
						PrintToChatAll("%d (%.0f%%): %s", iClient[i][DMG], (float(iClient[i][DMG]) / fTotalDamage) * 100.0, sName);
				}
			}
		}
		else {

			for (new i; i < iSurvivors; i++){

				if (g_iCvarPrivateFlags & (1 << _:WITCH))
					PrintToChat(iClient[i][INDEX], "%t", "Prodmg #14", g_iTotalDamage[iIndex][bTankBoss], i + 1, iClient[i][DMG], (float(iClient[i][DMG]) / fTotalDamage) * 100.0);
				else
					PrintToChatAll("%d (%.0f%%): %N", iClient[i][DMG], (float(iClient[i][DMG]) / fTotalDamage) * 100.0, iClient[i][INDEX]);
			}
		}
	}

	// reset var
	g_iTotalDamage[iIndex][bTankBoss] = 0;
}

public SortFuncByDamageDesc(x[], y[], const array[][], Handle:hndl)
{
	if (x[1] < y[1])
		return 1;
	else if (x[1] == y[1])
		return 0;

	return NULL;
}

Float:GetCoopMultiplie()
{
	decl String:sDifficulty[24];
	GetConVarString(g_hDifficulty, sDifficulty, 24);

	if (StrEqual(sDifficulty, "Easy"))
		return 0.75;
	else if (StrEqual(sDifficulty, "Normal"))
		return 1.0;

	return 2.0;
}

bool:IsVersusGameMode()
{
	decl String:sGameMode[12];
	GetConVarString(g_hGameMode, sGameMode, 12);
	return StrEqual(sGameMode, "versus");
}

public OnConvarChange_TankHealth(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_iCvarHealth[TANK] = RoundFloat(GetConVarFloat(g_hTankHealth) * (IsVersusGameMode() ? GetConVarFloat(g_hVsBonusHealth) : GetCoopMultiplie()));
}

public OnConvarChange_WitchHealth(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_iCvarHealth[WITCH] = GetConVarInt(convar);
}

public OnConvarChange_SkipBots(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_bCvarSkipBots = GetConVarBool(convar);
}

public OnConvarChange_SurvLimit(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_bCvar1v1Mode = GetConVarInt(convar) == 1 ? true : false;
}

public OnConvarChange_Flags(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_iCvarFlags = GetConVarInt(convar);
}

public OnConvarChange_Private(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_iCvarPrivateFlags = GetConVarInt(convar);
}

public OnConvarChange_RunAway(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (!StrEqual(oldValue, newValue))
		g_bCvarRunAway = GetConVarBool(convar);
}
