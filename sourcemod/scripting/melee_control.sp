#define PLUGIN_VERSION "1.1"

/*
 * ============================================================================
 *
 *  Description:    Adds convar to control melee shove function.
 *
 *  Credits:        Original code taken from Rotoblin2 project
 *                  written by Me.
 *                  See rotoblin.meleefatigue module (1.3 dev or higher version)
 *
 *  Site:           http://code.google.com/p/rotoblin2/
 *
 *  Copyright (C) 2012-2014 raziEiL <war4291@mail.ru>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */
 
#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <left4dhooks>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

public Plugin:myinfo =
{
	name = "[L4D] Melee Control",
	author = "raziEiL [disawar1]",
	description = "Adds convar to control melee shove function",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/raziEiL"
}

static g_iCvarMeleeControlFlags, bool:g_bCvarNoDeadStop, Handle:g_hPouncing[MPS];

public OnPluginStart()
{
	new Handle:hCvarMeleeControl = CreateConVar("rotoblin_melee_flags", "0", "Blocks melee effect on infected. Flag (add together): 0=Disable, 2=Smoker, 4=Boomer, 8=Hunter, 16=CI, 30=all", FCVAR_NOTIFY, true, 0.0, true, 30.0);
	new Handle:hCvarNoDeadStop = CreateConVar("rotoblin_melee_deadstop", "1", "Blocks deadstop feature", FCVAR_NOTIFY, true, 0.0, true, 1.0); // Broken on Linux after steampipe update!

	g_iCvarMeleeControlFlags = GetConVarInt(hCvarMeleeControl);
	g_bCvarNoDeadStop = GetConVarBool(hCvarNoDeadStop);

	HookConVarChange(hCvarMeleeControl, OnCvarChange_MeleeControl);
	HookConVarChange(hCvarNoDeadStop, OnCvarChange_NoDeadStop);

	HookEvent("ability_use", MC_ev_AbilityUse);
}

public MC_ev_AbilityUse(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_bCvarNoDeadStop) return;
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	if (g_hPouncing[client] == INVALID_HANDLE){

		decl String:sAbility[64];
		GetEventString(event, "ability", sAbility, 64);

		if (StrEqual(sAbility, "ability_lunge"))
			g_hPouncing[client] = CreateTimer(0.3, MC_t_GroundTouchCheck, client, TIMER_REPEAT);
	}
}

public Action:MC_t_GroundTouchCheck(Handle:timer, any:client)
{
	if (IsClientInGame(client)){

		if (!(GetEntityFlags(client) & FL_ONGROUND || !IsPlayerAlive(client)))
			return Plugin_Continue;
	}

	g_hPouncing[client] = INVALID_HANDLE;
	return Plugin_Stop;
}

public Action:L4D2_OnEntityShoved(client, entity, weapon, Float:vector[3])
{
	if ((g_iCvarMeleeControlFlags || g_bCvarNoDeadStop) && IsClient(client) && IsSurvivor(client)){

		if (IsClient(entity) && IsInfected(entity)){

			new iClass = GetPlayerClass(entity);

			if (iClass == ZC_HUNTER && g_hPouncing[entity] != INVALID_HANDLE){

				if (g_bCvarNoDeadStop){

					KillTimer(g_hPouncing[entity]);
					g_hPouncing[entity] = CreateTimer(0.2, MC_t_GroundTouchCheck, entity, TIMER_REPEAT);

					return Plugin_Handled;
				}
				return Plugin_Continue;
			}

			if (g_iCvarMeleeControlFlags & (1 << iClass))
				return Plugin_Handled;
		}
		else if (g_iCvarMeleeControlFlags & 16 && IsCommonInfected(entity))
			return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action:L4D_OnShovedBySurvivor(client, victim, const Float:vector[3])
{
	if (g_bCvarNoDeadStop && g_hPouncing[victim] != INVALID_HANDLE && GetClientTeam(victim) == 3 && GetPlayerClass(victim) == ZC_HUNTER)
		return Plugin_Handled;

	return Plugin_Continue;
}

public OnCvarChange_MeleeControl(Handle:convar, const String:oldValue[], const String:newValue[])
{
	g_iCvarMeleeControlFlags = GetConVarInt(convar);
}

public OnCvarChange_NoDeadStop(Handle:convar, const String:oldValue[], const String:newValue[])
{
	g_bCvarNoDeadStop = GetConVarBool(convar);
}
