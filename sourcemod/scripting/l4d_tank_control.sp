#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <left4dhooks>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

static		bool:g_bWasFlipped, bool:g_bLeftStartRoom, bool:g_bBug, bool:g_bSecondRound,
			queuedTank, String:tankSteamId[32], Handle:hTeamATanks, Handle:hTeamBTanks, Handle:g_hCvarSurvLimit;

public Plugin:myinfo = {
	name = "[L4D] Tank Control",
	author = "Jahze, vintik, raziEiL [disawar1]",
	version = "1.6",
	description = "Forces each player to play the tank once before resetting the pool."
};

public OnPluginStart()
{
	LoadTranslations("l4d_competitive.phrases");

	g_hCvarSurvLimit = FindConVar("survivor_limit");
	HookEvent("player_left_start_area", TC_ev_LeftStartAreaEvent, EventHookMode_PostNoCopy);
	HookEvent("round_end", TC_ev_RoundEnd, EventHookMode_PostNoCopy);
	HookEvent("player_team", TC_ev_OnTeamChange);

	RegConsoleCmd("sm_tank", Command_FindNexTank);

	hTeamATanks = CreateArray(32);
	hTeamBTanks = CreateArray(32);
}

public Action:Command_FindNexTank(client, args)
{
	if (!client || IsPluginDisabled()) return Plugin_Handled;

	new iTeam = GetClientTeam(client);
	if (iTeam == 1) {
		if (queuedTank) PrintToChat(client, "%t", "Tank_Control #1", queuedTank);
		if (g_bLeftStartRoom) PrintTankOwners(client);
		return Plugin_Handled;
	}
	for (new i = 1; i < MaxClients+1; i++) {
		if (IsClientInGame(i) && GetClientTeam(i) == iTeam) {
			if (queuedTank) PrintToChat(i, "%t", "Tank_Control #1", queuedTank);
		}
	}
	if (g_bLeftStartRoom) PrintTankOwners(client);
	return Plugin_Handled;
}

PrintTankOwners(client)
{
	new iMaxArrayA = GetArraySize(g_bWasFlipped ? hTeamBTanks : hTeamATanks);

	if (iMaxArrayA == 0)
		return;

	decl String:sTankSteamId[64], iInfected;

	PrintToConsole(client, "%t", "Tank_Control #2");

	for (new iIndex; iIndex < iMaxArrayA; iIndex++){

		GetArrayString(g_bWasFlipped ? hTeamBTanks : hTeamATanks, iIndex, sTankSteamId, sizeof(sTankSteamId));
		if ((iInfected = GetInfectedPlayerBySteamId(sTankSteamId))){

			PrintToConsole(client, "0%d. %N [%s]", iIndex + 1, iInfected, sTankSteamId);
		}
		else
			PrintToConsole(client, "0%d. (left the team) [%s]", iIndex + 1, sTankSteamId);
	}
}

public OnMapStart()
{
	g_bSecondRound = false;
	g_bBug = false;
	queuedTank = 0;
}

public TC_ev_RoundEnd(Handle:event, String:name[], bool:dontBroadcast)
{
	g_bSecondRound = true;
	queuedTank = 0;
	g_bLeftStartRoom = false;
}

public TC_ev_LeftStartAreaEvent(Handle:event, String:name[], bool:dontBroadcast)
{
	if (IsPluginDisabled()) return;

	if (FindConVar("l4d_team_manager_ver") != INVALID_HANDLE)
		FindValidTeam();
	else
		g_bWasFlipped = bool:GameRules_GetProp("m_bAreTeamsFlipped");

	queuedTank = 0;
	g_bLeftStartRoom = true;
	ChoseTankAndPrintWhoBecome();
}

public TC_ev_OnTeamChange(Handle:event, String:name[], bool:dontBroadcast)
{
	if (IsPluginDisabled()) return;

	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client && client == queuedTank)
		ChoseTankAndPrintWhoBecome();
}

public OnClientDisconnect(client)
{
	if (IsPluginDisabled()) return;

	if (client && client == queuedTank)
		ChoseTankAndPrintWhoBecome();
}

ChoseTankAndPrintWhoBecome()
{
	ChooseTank(true);
	if (queuedTank) PrintToChatAll("%t", "Tank_Control #1", queuedTank);
}

public Action:L4D_OnTryOfferingTankBot(tank_index, &bool:enterStatis)
{
	if (IsPluginDisabled()) 
		return Plugin_Continue;

	if (!IsFakeClient(tank_index)){

		for (new i=1; i <= MaxClients; i++) {
			if (!IsClientInGame(i))
				continue;

			if (GetClientTeam(i) == 2)
				continue;

			PrintHintText(i, "%t", "Tank_Control #4");
			PrintToChat(i, "%t", "Tank_Control #3", tank_index);
		}
		SetTankFrustration(tank_index, 100);
		L4D2Direct_SetTankPassedCount(L4D2Direct_GetTankPassedCount() + 1);

		return Plugin_Handled;
	}

	if (!queuedTank)
		ChoseTankAndPrintWhoBecome();

	if (queuedTank){

		ForceTankPlayer();
		PushArrayString(g_bWasFlipped ? hTeamBTanks : hTeamATanks, tankSteamId);
		queuedTank = 0;
	}

	return Plugin_Continue;
}

static bool:HasBeenTank(const String:SteamId[])
{
	return (FindStringInArray(g_bWasFlipped ? hTeamBTanks : hTeamATanks, SteamId) != -1);
}

static ChooseTank(bool:bFirstPass) {

	decl String:SteamId[32];
	new Handle:SteamIds = CreateArray(32);

	for (new i = 1; i < MaxClients+1; i++) {
		if (!IsClientConnected(i) || !IsClientInGame(i)) {
			continue;
		}

		GetClientAuthId(i, AuthId_Steam2, SteamId, sizeof(SteamId));

		if (IsFakeClient(i) || !IsInfected(i) || HasBeenTank(SteamId) || i == queuedTank) {
			continue;
		}

		PushArrayString(SteamIds, SteamId);
	}

	if (GetArraySize(SteamIds) == 0) {
		if (bFirstPass) {

			ClearArray(g_bWasFlipped ? hTeamBTanks : hTeamATanks);
			ChooseTank(false);
		}
		else queuedTank = 0;
		return;
	}

	new idx = GetRandomInt(0, GetArraySize(SteamIds)-1);
	GetArrayString(SteamIds, idx, tankSteamId, sizeof(tankSteamId));
	queuedTank = GetInfectedPlayerBySteamId(tankSteamId);
}

static ForceTankPlayer() {
	for (new i = 1; i < MaxClients+1; i++) {
		if (!IsClientConnected(i) || !IsClientInGame(i)) {
			continue;
		}

		if (IsInfected(i)) {
			if (queuedTank == i) {
				L4D2Direct_SetTankTickets(i, 1000);
			}
			else {
				L4D2Direct_SetTankTickets(i, 0);
			}
		}
	}
}

static GetInfectedPlayerBySteamId(const String:SteamId[]) {
	decl String:cmpSteamId[32];

	for (new i = 1; i < MaxClients+1; i++) {
		if (!IsClientConnected(i)) {
			continue;
		}

		if (!IsInfected(i)) {
			continue;
		}

		GetClientAuthId(i, AuthId_Steam2, cmpSteamId, sizeof(cmpSteamId));

		if (StrEqual(SteamId, cmpSteamId)) {
			return i;
		}
	}

	return 0;
}

static GetSurvivorPlayerBySteamId(const String:SteamId[]) {
	decl String:cmpSteamId[32];

	for (new i = 1; i < MaxClients+1; i++) {
		if (!IsClientConnected(i)) {
			continue;
		}

		if (!IsSurvivor(i)) {
			continue;
		}

		GetClientAuthId(i, AuthId_Steam2, cmpSteamId, sizeof(cmpSteamId));

		if (StrEqual(SteamId, cmpSteamId)) {
			return i;
		}
	}

	return 0;
}

bool:IsPluginDisabled()
{
	return GetConVarInt(g_hCvarSurvLimit) == 1;
}

// Support l4d scores
FindValidTeam()
{
	new bool:bWasFlipped;

	if (!g_bSecondRound)
		bWasFlipped = false;
	else
		bWasFlipped = true;

	g_bWasFlipped = bWasFlipped;

	if (!bWasFlipped){

		new iMaxArrayA = GetArraySize(hTeamATanks);
		new iMaxArrayB = GetArraySize(hTeamBTanks);

		if (!iMaxArrayA && !iMaxArrayB)
			return;

		decl String:sTankSteamId[64];

		new iMatchA, iMatchB, iNotMatchesA = iMaxArrayA, iNotMatchesB = iMaxArrayB;

		if (iMaxArrayA){

			for (new iIndex; iIndex < iMaxArrayA; iIndex++){

				GetArrayString(hTeamATanks, iIndex, sTankSteamId, sizeof(sTankSteamId));
				if (GetInfectedPlayerBySteamId(sTankSteamId)){
					iMatchA++;
				}
			}
		}
		if (iMaxArrayB){

			for (new iIndex; iIndex < iMaxArrayB; iIndex++){

				GetArrayString(hTeamBTanks, iIndex, sTankSteamId, sizeof(sTankSteamId));
				if (GetSurvivorPlayerBySteamId(sTankSteamId)){
					iMatchB++;
				}
			}
		}

		iNotMatchesA -= iMatchA;
		iNotMatchesB -= iMatchB;

		if (iNotMatchesA >= iMatchA && iNotMatchesB >= iMatchB){

			g_bWasFlipped = true;
			g_bBug = true;
		}
	}
	else if (bWasFlipped && g_bBug)
		g_bWasFlipped = false;
}
