#define PLUGIN_VERSION "1.4"

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

#define TAG "\x01[SM]"

public Plugin:myinfo =
{
	name = "[L4D] Custom Vote",
	author = "Scratchy [Laika], vintik, raziEiL [disawar1]",
	description = "Custom Vote Lib",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/raziEiL"
};

new templock,
	yesvotes,
	novotes,
	PlayerCountNoSpecs,
	bool:IsVoteInProcess,
	bool:IsClientVoted[MAXPLAYERS+1],
	Float:VoteMaxTime, lastVoteTime,
	Handle:cVarTimeout, Handle:cVarSpec,
	Handle:g_fwdOnVoteEnd, String:g_sIssue[96];

public OnPluginStart()
{
	VoteMaxTime = GetConVarFloat(FindConVar("sv_vote_timer_duration"));
	RegConsoleCmd("Vote",vote);
	RegConsoleCmd("callvote",VoteRequest);
	cVarTimeout = CreateConVar("sm_cvote_cooldown", "45", "How many seconds players have to wait between votes, 0 - disable the cooldawn", FCVAR_SPONLY, true, 0.0);
	cVarSpec = CreateConVar("sm_cvote_spec", "0", "Allows spec to vote", FCVAR_SPONLY, true, 0.0, true, 1.0);
	HookEvent("round_start", VM_ev_RoundStart, EventHookMode_PostNoCopy);
	RegAdminCmd("sm_cvote", Command_Cvote, ADMFLAG_ROOT);
	//AutoExecConfig(true, "cvote");
}

public Action:Command_Cvote(int client, int args)
{
	if (client){
		int time = GetTime();
		if (time < templock) {
			ReplyToCommand(client, "%s You have to wait other %d sec.", TAG, templock - time);
			return Plugin_Handled;
		}
		StartMatchVote(client, "Test Vote");
	}
	return Plugin_Handled;
}

public OnMapStart()
{
	IsVoteInProcess = false;
}

public Action:VM_ev_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!GameRules_GetProp("m_bInSecondHalfOfRound"))
	{
		IsVoteInProcess = false;
	}
}

public Action:VoteRequest(client, args)
{
	if (IsVoteInProcess)
	{
		PrintToChat(client, "%s You have to wait intil current vote ended", TAG);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("L4D_CustomVote", Native_L4D_CustomVote);
	g_fwdOnVoteEnd = CreateGlobalForward("L4D_OnCustomVoteEnd", ET_Ignore, Param_Cell, Param_String);
	RegPluginLibrary("cvote");
	return APLRes_Success;
}

public Native_L4D_CustomVote(Handle:plugin, numParams)
{
	int client = GetNativeCell(1);
	if (!IsClientAndInGame(client)) return false;
	
	int time = GetTime();
	if (time < templock) {
		ReplyToCommand(client, "%s You have to wait other %d sec.", TAG, templock - time);
		return false;
	}
	
	char sIssue[128];
	GetNativeString(2, sIssue, sizeof(sIssue));
	
	int CvarCoolDawn = GetConVarInt(cVarTimeout);

	if (CvarCoolDawn && lastVoteTime != 0)
	{
		if(time - lastVoteTime <= CvarCoolDawn)
		{
			ReplyToCommand(client, "%s You have to wait other %d sec.", TAG, lastVoteTime + CvarCoolDawn - GetTime());
			return false;
		}
	}
	if (GetClientTeam(client) == 1)
	{
		ReplyToCommand(client, "%s Spectators haven't right to vote.", TAG);
		return false;
	}

	return StartMatchVote(client, sIssue);
}

bool:StartMatchVote(client, const String:issue[])
{
	if (GetClientTeam(client) == 1)
	{
		PrintToChat(client, "%s Spectators have not right to vote", TAG);
		return false;
	}
	if (IsVoteInProcess)
	{
		PrintToChat(client, "%s You have to wait intil current vote ended", TAG);
		return false;
	}
	PlayerCountNoSpecs = GetPlayerCount(true);
	Event event = CreateEvent("vote_started", true);
	SetEventString(event,"issue","#L4D_TargetID_Player");
	SetEventString(event,"param1",issue);
	SetEventInt(event,"team", 0);
	SetEventInt(event,"initiator",0);
	FireEvent(event);
	yesvotes = novotes = 0;
	lastVoteTime = GetTime();
	IsVoteInProcess = true;
	CreateTimer(VoteMaxTime, VoteTime, _, TIMER_FLAG_NO_MAPCHANGE);
	FakeClientCommand(client, "Vote Yes");
	strcopy(SZF(g_sIssue), issue);
	return true;
}

UpdateVotes()
{
	PrintToServer("vote info: yes=%d, no=%d, total=%d", yesvotes, novotes, PlayerCountNoSpecs);
	Event event = CreateEvent("vote_changed", true);
	SetEventInt(event,"yesVotes",yesvotes);
	SetEventInt(event,"noVotes",novotes);
	SetEventInt(event,"potentialVotes",PlayerCountNoSpecs);
	FireEvent(event);
	FinishVote();
}

FinishVote()
{
	if (yesvotes+novotes == PlayerCountNoSpecs || !IsVoteInProcess)
	{
		PrintToServer("voting complete!");
		CreateTimer(2.5, VoteEnded, (yesvotes > novotes), TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action:VoteEnded(Handle:timer, any:IsVotePassed)
{
	if (IsVotePassed)
	{
		Event event = CreateEvent("vote_passed", true);
		SetEventString(event,"details","#L4D_TargetID_Player");
		SetEventString(event,"param1","Vote passed!");
		SetEventInt(event,"team",0);
		FireEvent(event);
	}
	else
	{
		Event event = CreateEvent("vote_failed", true);
		SetEventInt(event,"team",0);
		FireEvent(event);
	}
	Event event = CreateEvent("vote_ended", true);
	FireEvent(event);
	templock = GetTime() + 8;
	IsVoteInProcess = false;
	for (new i = 0; i <= MaxClients; i++)
	{
		IsClientVoted[i] = false;
	}
	Call_StartForward(g_fwdOnVoteEnd);
	Call_PushCell(IsVotePassed);
	Call_PushString(g_sIssue);
	Call_Finish();
}

public Action:VoteTime(Handle:timer)
{
	if (IsVoteInProcess)
	{
		IsVoteInProcess = false;
		FinishVote();
	}
}

// IsSpectators не считать спектров?
GetPlayerCount(bool:IsSpectators = true)
{
	new j=0;
	for (new i = 1; i<=MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i) && (IsSpectators ? GetClientTeam(i) != 1 : true))
		{
			j++;
		}
	}
//	PrintToChatAll("Account of players is %i", j);
	return j;
}

public OnClientDisconnect(client)
{
	IsClientVoted[client] = false;
}

public Action:vote(client, args)
{
	if (!client || IsClientVoted[client]) return Plugin_Handled;
	if (IsVoteInProcess && !GetConVarBool(cVarSpec) && GetClientTeam(client) == 1) return Plugin_Handled;
	if (!IsVoteInProcess) return Plugin_Continue;
	decl String:arg[8];
	GetCmdArg(1,arg,8);
	PrintToServer("%s Got vote %s from %N", TAG, arg,client);
	if (strcmp(arg,"Yes",false) == 0)
	{
		yesvotes++;
		IsClientVoted[client] = true;
	}
	else if (strcmp(arg,"No",false) == 0)
	{
		novotes++;
		IsClientVoted[client] = true;
	}

	UpdateVotes();
	return Plugin_Continue;
}