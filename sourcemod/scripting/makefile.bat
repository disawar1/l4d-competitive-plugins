@echo off
title Build Competitive plugins project
setlocal
::==================================================::
::             CONFIGURATION SETTINGS               ::
::            Your folder replace after =           ::
::==================================================::
:: Note: For compiling you'll need include files.
:: More information here https://bitbucket.org/disawar1/l4d-competitive-plugins

:: Patch to the SourceMod scripting folder
set SPCOMP=G:\srcds\Pawn\fresh\sm-1.10.0\addons\sourcemod\scripting\spcomp.exe

:: Script
echo ##############################################################
echo #           Competitive Plugins Compiler Helper              #
echo #                          * * *                             #
echo # Script has been started and make your life easier!         #
echo # It take some time...                                       #
echo #                             Written by raziEiL [disawar1]  #
echo ##############################################################

set RELEASE_DIR=compiled
set LOG=%RELEASE_DIR%\log.log
:: удалить папку
rd /s /q %RELEASE_DIR%
:: создать папку
md %RELEASE_DIR%
:: логирование в файл
call :LOG > %LOG%
:LOG
:: справка по команде for https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb490909(v=technet.10)?redirectedfrom=MSDN
for %%i in (*) do (
	if %%~xi==.sp (
		@echo on
		echo %%~ni
		%SPCOMP% %%i -o %RELEASE_DIR%\%%~ni.smx
		echo.
		@echo off
	)
)
endlocal