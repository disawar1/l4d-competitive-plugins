#define PLUGIN_VERSION "1.3"

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#undef REQUIRE_PLUGIN
#include <l4d_lib>

static const String:g_sStartMapList[][36] =
{
	"l4d_vs_hospital01_apartment",
	"l4d_garage01_alleys",
	"l4d_vs_airport01_greenhouse",
	"l4d_vs_smalltown01_caves",
	"l4d_vs_farm01_hilltop",
	"l4d_river01_docks",
	// l4d coop
	"l4d_hospital01_apartment",
	"l4d_garage01_alleys",
	"l4d_airport01_greenhouse",
	"l4d_smalltown01_caves",
	"l4d_farm01_hilltop",
	"l4d_river01_docks",
	// l4d2
	"c1m1_hotel",
	"c2m1_highway",
	"c3m1_plankcountry",
	"c4m1_milltown_a",
	"c5m1_waterfront",
	"c6m1_riverbank",
	"c13m1_alpinecreek",
	"c8m1_apartment",
	"c9m1_alleys",
	"c10m1_caves",
	"c11m1_greenhouse",
	"c12m1_hilltop",
	"c7m1_docks"
};

static const String:g_sFinalMapList[][36] =
{
	"l4d_vs_hospital05_rooftop",
	"l4d_garage02_lots",
	"l4d_vs_airport05_runway",
	"l4d_vs_smalltown05_houseboat",
	"l4d_vs_farm05_cornfield",
	"l4d_river03_port",
	// l4d coop
	"l4d_hospital05_rooftop",
	"l4d_garage02_lots",
	"l4d_airport05_runway",
	"l4d_smalltown05_houseboat",
	"l4d_farm05_cornfield",
	"l4d_river03_port",
	// l4d2
	"c1m4_atrium",
	"c2m5_concert",
	"c3m4_plantation",
	"c4m5_milltown_escape",
	"c5m5_bridge",
	"c6m3_port",
	"c13m4_cutthroatcreek",
	"c8m5_rooftop",
	"c9m2_lots",
	"c10m5_houseboat",
	"c11m5_runway",
	"c12m5_cornfield",
	"c7m3_port"
};

public Plugin:myinfo =
{
	name = "[L4D & L4D2] Next Campaign",
	author = "raziEiL [disawar1]",
	description = "Switches campaign to the next one in versus/coop mode (supports custom campaign)",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/raziEiL"
}

static bool:g_bIsFinal, bool:g_bReadyToChange, g_iMapIndex, g_iStartIndex, g_iTotalCampaigns = 6;

public OnPluginStart()
{
	if (!IsL4DEngine()){

		g_iStartIndex = 12;
		g_iTotalCampaigns = 25;
	}
	else if (!IsVersusMode()){

		g_iStartIndex = 6;
		g_iTotalCampaigns = 12;
	}

	HookEvent("round_end", NC_ev_RoundEnd, EventHookMode_PostNoCopy);
	HookEvent("finale_win", NC_ev_FinaleWin, EventHookMode_PostNoCopy);
}

public OnMapStart()
{
	if ((g_bIsFinal = IsFinalMap())){

		g_bReadyToChange = false;
		g_iMapIndex = 0;

		decl String:sMap[64];
		GetCurrentMap(sMap, 64);

		for (new index = g_iStartIndex; index < g_iTotalCampaigns; index++){

			if (StrEqual(sMap, g_sFinalMapList[index])){

				g_iMapIndex = index + 1;
				break;
			}
		}
	}
}
// co-op
public NC_ev_FinaleWin(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!IsVersusMode())
		CreateTimer(6.0, NC_t_UnscrambleSupports, _, TIMER_FLAG_NO_MAPCHANGE);
}
// vs
public NC_ev_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (g_bIsFinal && IsVersusMode() && GameRules_GetProp("m_bInSecondHalfOfRound")){

		if (g_bReadyToChange)
			CreateTimer(0.1, NC_t_UnscrambleSupports, _, TIMER_FLAG_NO_MAPCHANGE); // short timer to make sure that unscramble module finished its job
		else
			g_bReadyToChange = true;
	}
}

public Action:NC_t_UnscrambleSupports(Handle:timer)
{
	ForceChangeLevel(g_sStartMapList[g_iMapIndex >= g_iTotalCampaigns ? g_iStartIndex : g_iMapIndex], "by Next Campaign plugin");
}
